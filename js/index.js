$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $("[data-toggle='popover']").popover();
  $('.carousel').carousel({
    interval: 1000
  });

  $('#exampleModal').on('show.bs.modal', function (e){
      console.log('El modal se está mostrando');

      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disable', true);


  });

  $('#exampleModal').on('shown.bs.modal', function (e){
    console.log('El modal se mostró');
  });

  $('#exampleModal').on('hide.bs.modal', function (e){
    console.log('El modal se está ocultando');

    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-outline-success');
    $('#contactoBtn').prop('disable', false);
  });

  $('#exampleModal').on('hidden.bs.modal', function (e){
    console.log('El modal se ocultó');
  });
});